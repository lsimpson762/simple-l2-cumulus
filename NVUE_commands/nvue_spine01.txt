cumulus@spine01:mgmt:~$ nv config show -o commands
nv set interface bond12 bond member swp1
nv set interface bond12 bond member swp2
nv set interface bond12 bridge domain br_default access 10
nv set interface bond12 type bond
nv set interface swp1-2 type swp
nv set interface vlan10 ip address 192.168.100.1/24
nv set interface vlan10 type svi
nv set interface vlan10 vlan 10
nv set system hostname spine01
