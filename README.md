# simple-l2-cumulus

## A Simple Layer 2 topology with Cumulus Linux

This repository contains the configuration files for a simple L2 topology with one spine, two leaf nodes and one Ubuntu server. All connections from server to leaf to spine are Layer 2 VLAN. The spine contains one Layer 3 SVI for IP connectivity to the server. MLAG is utilized from spine to leaf and from leaf to server providing redundant Layer 2 connectivity.

The NVUE `startup.yaml` files for the Cumulus Linux switches are stored in the `config_files` directory along with the `config.yaml` Netplan configuration for the Ubuntu server.

The NVUE commands for each switch are stored in the `NVUE_commands` directory.

## IPAM

- VLAN 10 - 192.168.100.1/24 - SVI on Spine01
- SRV1 - 192.168.100.100 - untagged

## Want to try it out?
This example was built in [NVIDIA Air](http://air.nvidia.com). The `simple-l2-cumulus.json` file can be loaded into the Air simulation builder tool to create the topology and build the simulation.

The topology of the environment looks like this:

![topology](simpleL2.svg)